const express = require('express');
var router = express.Router();

//Image Upload
var UploadController = require('../middlewares/upload');


var AdminController = require('../controllers/AdminController');
//var CategoryController = require('../controllers/CategoryController')


var ProductController = require('../controllers/ProductController');
var BrandController = require('../controllers/BrandController');

var DialColorController = require('../controllers/DialColorController');
var FunctionCategoryController = require('../controllers/FunctionCategoryController');
var StrapColorController = require('../controllers/StrapColorController');
var StrapMaterialController = require('../controllers/StrapMaterialController');
var WatchStyleController = require('../controllers/WatchStyleController');
var WatchTypeController = require('../controllers/WatchTypeController');


var UserController = require('../controllers/UserController');


router.post('/login', AdminController.login);
//router.get('/logout', AdminController.logout);

// Admin Controller
router.post('/addAdminUser',AdminController.addAdminUser);
router.get('/getAdminUser',AdminController.getAdminUser);
router.delete('/deleteAdminUser',AdminController.deleteAdminUser);

// category
/*router.post('/addCategory',CategoryController.addCategory);
router.get('/getCategory',CategoryController.getCategory);
router.delete('/deleteCategory',CategoryController.deleteCategory);
*/


// Product
router.post('/addProduct',ProductController.addProduct);
router.get('/getProduct',ProductController.getProduct);
router.delete('/deleteProduct',ProductController.deleteProduct);

// Brand
router.post('/addBrand',UploadController.uploadImages,BrandController.addBrand);
router.get('/getBrand',BrandController.getBrand);
router.delete('/deleteBrand',BrandController.deleteBrand);

// Dial Color Controller
router.post('/addDialColor',DialColorController.addDialColor);
router.get('/getDialColor',DialColorController.getDialColor);
router.delete('/deleteDialColor',DialColorController.deleteDialColor);

// Function Category Controller
router.post('/addFunctionCategory',FunctionCategoryController.addFunctionCategory);
router.get('/getFunctionCategory',FunctionCategoryController.getFunctionCategory);
router.delete('/deleteFunctionCategory',FunctionCategoryController.deleteFunctionCategory);

// Strap Color Controller
router.post('/addStrapColor',StrapColorController.addStrapColor);
router.get('/getStrapColor',StrapColorController.getStrapColor);
router.delete('/deleteStrapColor',StrapColorController.deleteStrapColor);

// Strap Material Controller
router.post('/addStrapMaterial',StrapMaterialController.addStrapMaterial);
router.get('/getStrapMaterial',StrapMaterialController.getStrapMaterial);
router.delete('/deleteStrapMaterial',StrapMaterialController.deleteStrapMaterial);

// Watch Style Controller
router.post('/addWatchStyle',WatchStyleController.addWatchStyle);
router.get('/getWatchStyle',WatchStyleController.getWatchStyle);
router.delete('/deleteWatchStyle',WatchStyleController.deleteWatchStyle);

// Watch Type Controller
router.post('/addWatchType',WatchTypeController.addWatchType);
router.get('/getWatchType',WatchTypeController.getWatchType);
router.delete('/deleteWatchType',WatchTypeController.deleteWatchType);

//User Get 
router.get('/getUsers',UserController.getUsers);
router.delete('/deleteUsers',UserController.deleteUsers);

module.exports = router;
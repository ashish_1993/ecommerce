const express = require('express');
var router = express.Router();

//Image Upload
var UploadController = require('../middlewares/upload');


var UserController = require('../controllers/UserController');



router.post('/login', UserController.login);
router.post('/register', UserController.register);

//router.get('/logout', UserController.logout);


router.post('/addUserAddress', UserController.addUserAddress);
router.get('/getUserAddress', UserController.getUserAddress);
//router.put('/editUserAddress', UserController.editUserAddress);

router.delete('/deleteUserAddress', UserController.deleteUserAddress);

module.exports = router;
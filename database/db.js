const mysql = require('mysql');
const util = require( 'util' );
var config = {
  host: '127.0.0.1',
  user: 'root',
  port:3306,
  connectionLimit:10,
  password: '',
  database: 'watchbarzzer'
};

  function makeDb( config ) {
    const connection = mysql.createPool( config );
    return {
      query( sql, args ) {
        return util.promisify( connection.query )
          .call( connection, sql, args );
      },
      close() {
        return util.promisify( connection.end ).call( connection );
      }
    };
  }
  const db= makeDb(config)
module.exports = db;

const express =  require('express');
const app = express();
const bodyparser = require('body-parser');
const port = 9000;

var superAdmin = require('./routes/superAdmin');
var userRoutes =  require('./routes/UserRoutes');
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true, limit:'5mb' }));


app.use((req , res , next )=> {
	res.setHeader('Access-Control-Allow-Origin' ,'*');
	res.setHeader('Access-Control-Allow-Methods' ,'GET , POST , PUT , PATCH , DELETE');
	res.setHeader('Access-Control-Allow-Headers' ,'Content-Type, X-Requested-With , Accept , Origin, authorization');
	res.setHeader('Access-Control-Expose-Headers' , 'authorization');
	next();

});
app.use('/admin',superAdmin);
app.use('/user',userRoutes);
app.listen(port,(res)=>{
    console.log(`listening on ${port}` )
});





-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 07, 2020 at 01:27 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `watchbarzzer`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin_user`
--

CREATE TABLE `tbl_admin_user` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `token` text,
  `address` text,
  `password` text NOT NULL,
  `lastlogin` datetime DEFAULT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(100) NOT NULL,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin_user`
--

INSERT INTO `tbl_admin_user` (`id`, `name`, `email`, `mobile`, `image`, `token`, `address`, `password`, `lastlogin`, `createdAt`, `status`, `updatedAt`) VALUES
(1, 'superuser', 'admin@example.com', '8302653003', NULL, NULL, NULL, '$2a$10$b00HevmTgD5.jHqjbERPNe.Wap2BkvC/MGcP.wjPSZjUHr7yTtlHa', NULL, '2020-04-06 10:17:05', 'Active', '2020-04-06 10:17:05');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_brand`
--

CREATE TABLE `tbl_brand` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_brand`
--

INSERT INTO `tbl_brand` (`id`, `name`, `description`, `image`, `status`, `createdAt`) VALUES
(1, 'FBB', 'FBB', '', 'Active', '2020-04-07 00:00:04');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_products`
--

CREATE TABLE `tbl_products` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_description` text NOT NULL,
  `brand_id` varchar(11) NOT NULL,
  `dial_color` varchar(11) NOT NULL,
  `function_category` varchar(11) NOT NULL,
  `strap_color` varchar(11) NOT NULL,
  `strap_material` varchar(11) NOT NULL,
  `watch_style` varchar(11) NOT NULL,
  `watch_type` varchar(11) NOT NULL,
  `available` varchar(100) NOT NULL,
  `price` varchar(255) NOT NULL,
  `quantity` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_images`
--

CREATE TABLE `tbl_product_images` (
  `product_image_id` int(11) NOT NULL,
  `product_image_name` varchar(255) NOT NULL,
  `product_id` varchar(100) NOT NULL,
  `status` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `password` text NOT NULL,
  `status` varchar(100) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `fname`, `lname`, `email`, `mobile`, `password`, `status`, `createdAt`, `image`) VALUES
(1, 'ashish', 'sharma', 'user@example.com', '8302653003', '$2a$10$162.8FrMxGDGkZUl/ftGc.Q60I5Q1GxlItQc67vpNkmwEfPfJ/GQm', 'Active', '2020-04-06 17:03:04', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_address`
--

CREATE TABLE `tbl_user_address` (
  `id` int(11) NOT NULL,
  `user_id` varchar(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `state` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `location` text NOT NULL,
  `pincode` varchar(100) NOT NULL,
  `latitude` varchar(100) NOT NULL,
  `longitude` varchar(100) NOT NULL,
  `status` varchar(200) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_address`
--

INSERT INTO `tbl_user_address` (`id`, `user_id`, `name`, `mobile`, `state`, `city`, `address`, `location`, `pincode`, `latitude`, `longitude`, `status`, `createdAt`) VALUES
(1, '1', 'Rai', '1234567890', 'Rajasthan', 'Jaipur', '1878,  Khjano walo rasta, Ajmeri Gate', '', '332403', '', '', 'Active', '2020-04-06 22:55:29'),
(2, '1', 'Ashish', '1234567890', 'Rajasthan', 'Jaipur', 'Main Market, Sanjay', '', '332403', '', '', 'Active', '2020-04-06 22:59:46'),
(3, '', 'Ashish', '1234567890', 'Rajasthan', 'Jaipur', 'Main Market, Sanjay', '', '332403', '', '', 'Active', '2020-04-06 23:12:41'),
(4, '1', 'Ashish', '1234567890', 'Rajasthan', 'Jaipur', 'Main Market, Sanjay', '', '332403', '', '', 'Active', '2020-04-06 23:22:13');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_watch_dial_color`
--

CREATE TABLE `tbl_watch_dial_color` (
  `dial_color_id` int(11) NOT NULL,
  `dial_color_name` varchar(255) NOT NULL,
  `dial_color_code` varchar(255) NOT NULL,
  `status` varchar(100) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_watch_functions_category`
--

CREATE TABLE `tbl_watch_functions_category` (
  `function_id` int(11) NOT NULL,
  `function_name` varchar(255) NOT NULL,
  `function_description` text NOT NULL,
  `status` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_watch_strap_color`
--

CREATE TABLE `tbl_watch_strap_color` (
  `strap_color_id` int(11) NOT NULL,
  `strap_color_name` varchar(255) NOT NULL,
  `strap_color_code` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_watch_strap_material`
--

CREATE TABLE `tbl_watch_strap_material` (
  `strap_material_id` int(11) NOT NULL,
  `strap_material_name` varchar(255) NOT NULL,
  `status` varchar(100) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_watch_style`
--

CREATE TABLE `tbl_watch_style` (
  `style_id` int(11) NOT NULL,
  `style_name` varchar(255) NOT NULL,
  `status` varchar(100) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_watch_type`
--

CREATE TABLE `tbl_watch_type` (
  `type_id` int(11) NOT NULL,
  `type_name` varchar(255) NOT NULL,
  `status` varchar(100) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin_user`
--
ALTER TABLE `tbl_admin_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_brand`
--
ALTER TABLE `tbl_brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_products`
--
ALTER TABLE `tbl_products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `tbl_product_images`
--
ALTER TABLE `tbl_product_images`
  ADD PRIMARY KEY (`product_image_id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user_address`
--
ALTER TABLE `tbl_user_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_watch_dial_color`
--
ALTER TABLE `tbl_watch_dial_color`
  ADD PRIMARY KEY (`dial_color_id`);

--
-- Indexes for table `tbl_watch_functions_category`
--
ALTER TABLE `tbl_watch_functions_category`
  ADD PRIMARY KEY (`function_id`);

--
-- Indexes for table `tbl_watch_strap_color`
--
ALTER TABLE `tbl_watch_strap_color`
  ADD PRIMARY KEY (`strap_color_id`);

--
-- Indexes for table `tbl_watch_strap_material`
--
ALTER TABLE `tbl_watch_strap_material`
  ADD PRIMARY KEY (`strap_material_id`);

--
-- Indexes for table `tbl_watch_style`
--
ALTER TABLE `tbl_watch_style`
  ADD PRIMARY KEY (`style_id`);

--
-- Indexes for table `tbl_watch_type`
--
ALTER TABLE `tbl_watch_type`
  ADD PRIMARY KEY (`type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin_user`
--
ALTER TABLE `tbl_admin_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_brand`
--
ALTER TABLE `tbl_brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_products`
--
ALTER TABLE `tbl_products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_product_images`
--
ALTER TABLE `tbl_product_images`
  MODIFY `product_image_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_user_address`
--
ALTER TABLE `tbl_user_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_watch_dial_color`
--
ALTER TABLE `tbl_watch_dial_color`
  MODIFY `dial_color_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_watch_functions_category`
--
ALTER TABLE `tbl_watch_functions_category`
  MODIFY `function_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_watch_strap_color`
--
ALTER TABLE `tbl_watch_strap_color`
  MODIFY `strap_color_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_watch_strap_material`
--
ALTER TABLE `tbl_watch_strap_material`
  MODIFY `strap_material_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_watch_style`
--
ALTER TABLE `tbl_watch_style`
  MODIFY `style_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_watch_type`
--
ALTER TABLE `tbl_watch_type`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

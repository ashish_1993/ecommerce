var db = require('../database/Db');
var errorMsgs=require('../config/ErrorMsg');

exports.addStrapColor = async function addStrapColor(req,res){
 
    let strapColor= await db.query("insert into tbl_watch_strap_color(strap_color_name, strap_color_code,status) values (?,?,?)",[req.body.strap_color_name,req.body.strap_color_code,req.body.status]);
    if(strapColor!==undefined && strapColor!==null){
        if(strapColor['affectedRows']==1){
            return res.send(errorMsgs.customMsg(1,"Success!!! Strap Color Added Successfully.",{}));
        }
        else{
            return res.send(errorMsgs.customMsg(-1,"Sorry!!! Strap Color can't be Added. Try Again.",{}));
        }
    }
}



exports.getStrapColor = async function getStrapColor(req,res){
    var whereClause = req.query;
    let strapColor = [];
    
    if(Object.keys(whereClause).length && (whereClause.hasOwnProperty("strap_color_id") || whereClause.strap_color_id == null || whereClause.strap_color_id == "")){
        strapColor= await db.query("select * from tbl_watch_strap_color where strap_color_id=?",[whereClause.strap_color_id]);
    }else if(Object.keys(whereClause).length && whereClause.hasOwnProperty("status")){
        strapColor= await db.query("select * from tbl_watch_strap_color where status=?",[whereClause.status]);    
    }
    else{
        strapColor= await db.query("select * from tbl_watch_strap_color");
    }
    
    return res.send(errorMsgs.customMsg1(1,"Data Found","strapColor",strapColor));
}


exports.deleteStrapColor = async function deleteStrapColor(req,res){
    let strapColorDelete= await db.query("DELETE FROM `tbl_watch_strap_color` WHERE strap_color_id=?",[req.body.strap_color_id]);

    if(strapColorDelete!==undefined && strapColorDelete!==null){
        if(strapColorDelete['affectedRows']==1){
            return res.send(errorMsgs.customMsg(1,"Success!!! Strap Color Deleted Successfully.",{}));
        }
        else{
            return res.send(errorMsgs.customMsg(-1,"Sorry!!! Strap Color can't be Deleted. Try Again.",{}));
        }
    }
}


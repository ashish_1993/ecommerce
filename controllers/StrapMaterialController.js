var db = require('../database/Db');
var errorMsgs=require('../config/ErrorMsg');

exports.addStrapMaterial = async function addStrapMaterial(req,res){
 
    let strapMaterial= await db.query("insert into tbl_watch_strap_material(strap_material_name,status) values (?,?)",[req.body.strap_material_name,req.body.status]);
    if(strapMaterial!==undefined && strapMaterial!==null){
    	if(strapMaterial['affectedRows']==1){
            return res.send(errorMsgs.customMsg(1,"Success!!!Strap Material Added Successfully.",{}));
        }
        else{
            return res.send(errorMsgs.customMsg(-1,"Sorry!!! Strap Material can't be Added. Try Again.",{}));
        }
    }
}



exports.getStrapMaterial = async function getStrapMaterial(req,res){
	var whereClause = req.query;
	let strapMaterial = [];
	
	if(Object.keys(whereClause).length && (whereClause.hasOwnProperty("    strap_material_id") || whereClause.   strap_material_id == null || whereClause.    strap_material_id == "")){
		strapMaterial= await db.query("select * from tbl_watch_strap_material where   strap_material_id=?",[whereClause.  strap_material_id]);
	}else if(Object.keys(whereClause).length && whereClause.hasOwnProperty("status")){
        strapMaterial= await db.query("select * from tbl_watch_strap_material where status=?",[whereClause.status]);    
    }
    else{
		strapMaterial= await db.query("select * from tbl_watch_strap_material");
	}
    
    return res.send(errorMsgs.customMsg1(1,"Data Found","strapMaterial",strapMaterial));
}


exports.deleteStrapMaterial = async function deleteStrapMaterial(req,res){
	let strapMaterialDelete= await db.query("DELETE FROM `tbl_watch_strap_material` WHERE  strap_material_id=?",[req.body.    strap_material_id]);

    if(strapMaterialDelete!==undefined && strapMaterialDelete!==null){
    	if(strapMaterialDelete['affectedRows']==1){
            return res.send(errorMsgs.customMsg(1,"Success!!! Strap Material Deleted Successfully.",{}));
        }
        else{
            return res.send(errorMsgs.customMsg(-1,"Sorry!!! Strap Material can't be Deleted. Try Again.",{}));
        }
    }
}


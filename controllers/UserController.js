var db = require('../database/Db');
var bcrypt = require('bcryptjs');
var errorMsgs=require('../config/ErrorMsg');


exports.login = async function login(req,res){
	try{
        let resu = await db.query("select * from tbl_users where email=?",[req.body.email]);
        //console.log(resu)
        if(resu.length != 0){
        	if(resu[0]['status'] == 'Active'){
	        	if(bcrypt.compare(req.body.password,resu[0]['password'],(err,result)=>{
		            if(err){
		                console.log(err);
		                return res.status(403).send(errorMsgs.customMsg(-1,"Something went wrong",{}));
		            }
		            if(result){
		            	delete resu[0].password;
		                 return res.send(errorMsgs.customMsg1(1, 'Login Successfully',"info",resu[0]));
		            }
		            else{
		                return res.send(errorMsgs.customMsg(0,"Invalid Credentials",{}));
		            }
		        }
		        ));
	        }else{
	        	 return res.send(errorMsgs.customMsg(0,"Please User Your Profile Not Active!!!",{}));
	        }
        }else{
        	return res.send(errorMsgs.customMsg(0,"User Not Exits!!!",{}));
        }
        
    }catch(err){
        return res.send(err);
    }		
		
}

exports.register = async function register(req,res){
	try{
        let user = await db.query("select * from tbl_users where email=?",[req.body.email]);
        //console.log(resu)

        if(user.length == 0){
        	bcrypt.genSalt(10,(err,salt)=>{
		        if(err){
		            return res.status(403).send(err);
		        }
		        bcrypt.hash(`${req.body.password}`,salt,(err,hash)=>{
		            if(err){
		                return res.status(403).send(err);
		            }
		    		registerUser(req,res,hash);	
				});
			});
        }else{
        	return res.send(errorMsgs.customMsg(0,"User Already Exits!!!",{}));
        }        
    }catch(err){
        return res.send(err);
    }		
		
}


async function registerUser(req, res, hash){
	let userAdd= await db.query("insert into tbl_users(fname,lname,email,mobile,password, status) values (?,?,?,?,?,?)",[req.body.fname,req.body.lname,req.body.email,req.body.mobile,hash,req.body.status]);
    if(userAdd!==undefined && userAdd!==null){
    	if(userAdd['affectedRows']==1){
            return res.send(errorMsgs.customMsg(1,"Success!!! Register Successfully.",{}));
        }
        else{
            return res.send(errorMsgs.customMsg(-1,"Sorry!!! Please Try Again.",{}));
        }
    }
}


exports.getUsers = async function getUsers(req,res){
	var whereClause = req.query;
	let users = [];
	
	if(Object.keys(whereClause).length && (whereClause.hasOwnProperty("id") || whereClause.id == null || whereClause.id == "")){
		users= await db.query("select * from tbl_users where id=?",[whereClause.id]);
	}else if(Object.keys(whereClause).length && whereClause.hasOwnProperty("status")){
        users= await db.query("select * from tbl_users where status=?",[whereClause.status]);    
    }
    else{
		users= await db.query("select * from tbl_users");
	}
    
    return res.send(errorMsgs.customMsg1(1,"Data Found","users",users));
}

exports.deleteUsers = async function deleteUsers(req,res){
	let usersDelete= await db.query("DELETE FROM `tbl_users` WHERE id=?",[req.body.id]);

    if(usersDelete!==undefined && usersDelete!==null){
    	if(usersDelete['affectedRows']==1){
            return res.send(errorMsgs.customMsg(1,"Success!!! User Deleted Successfully.",{}));
        }
        else{
            return res.send(errorMsgs.customMsg(-1,"Sorry!!! User can't be Deleted. Try Again.",{}));
        }
    }
}

exports.addUserAddress = async function addUserAddress(req,res){
	//console.log(req.body)
	if(req.body.hasOwnProperty('user_id') && req.body.user_id != ''){

		let address= await db.query("insert into tbl_user_address(user_id, name, mobile, state, city, address, location, pincode, latitude, longitude, status) values (?,?,?,?,?,?,?,?,?,?,?)",[req.body.user_id,req.body.name,req.body.mobile,req.body.state,req.body.city,req.body.address,req.body.location,req.body.pincode,req.body.lat,req.body.long, req.body.status]);
	    if(address!==undefined && address!==null){
	    	if(address['affectedRows']==1){
	            return res.send(errorMsgs.customMsg(1,"Success!!! Address Added Successfully.",{}));
	        }
	        else{
	            return res.send(errorMsgs.customMsg(0,"Sorry!!! Address can't be Added. Try Again.",{}));
	        }
	    }
	}else{
		return res.send(errorMsgs.customMsg(0,"Sorry!!! Address can't be Added User not Selected. Try Again.",{}));
	}

    
}

exports.getUserAddress = async function getUserAddress(req,res){
	var whereClause = req.query;
	let address = [];
	
	if(Object.keys(whereClause).length && (whereClause.hasOwnProperty("user_id") || whereClause.user_id == null || whereClause.user_id == "")){
		address= await db.query("select * from tbl_user_address where user_id=?",[whereClause.user_id]);
	}
    
    return res.send(errorMsgs.customMsg1(1,"Data Found","address",address));
}


exports.deleteUserAddress = async function deleteUserAddress(req,res){
	let addressDelete= await db.query("DELETE FROM `tbl_user_address` WHERE id=?",[req.body.id]);

    if(addressDelete!==undefined && addressDelete!==null){
    	if(addressDelete['affectedRows']==1){
            return res.send(errorMsgs.customMsg(1,"Success!!! Address Deleted Successfully.",{}));
        }
        else{
            return res.send(errorMsgs.customMsg(-1,"Sorry!!! Address can't be Deleted. Try Again.",{}));
        }
    }
}




var db = require('../database/Db');
var errorMsgs=require('../config/ErrorMsg');

exports.addCategory = async function addCategory(req,res){

    let category= await db.query("insert into tbl_category(name, status) values (?,?)",[req.body.name,req.body.status]);
    if(category!==undefined && category!==null){
    	if(category['affectedRows']==1){
            return res.send(errorMsgs.customMsg(1,"Success!!! Category Added Successfully.",{}));
        }
        else{
            return res.send(errorMsgs.customMsg(-1,"Sorry!!! Category can't be Added. Try Again.",{}));
        }
    }
}



exports.getCategory = async function getCategory(req,res){
	var whereClause = req.query;
	let category = [];
	
	if(Object.keys(whereClause).length && (whereClause.hasOwnProperty("id") || whereClause.id == null || whereClause.id == "")){
		category= await db.query("select * from tbl_category where id=?",[whereClause.id]);
	}else if(Object.keys(whereClause).length && whereClause.hasOwnProperty("status")){
        category= await db.query("select * from tbl_category where status=?",[whereClause.status]);    
    }
    else{
		category= await db.query("select * from tbl_category");
	}
    
    return res.send(errorMsgs.customMsg1(1,"Data Found","category",category));
}


exports.deleteCategory = async function deleteCategory(req,res){
	let categoryDelete= await db.query("DELETE FROM `tbl_category` WHERE id=?",[req.body.id]);

    if(categoryDelete!==undefined && categoryDelete!==null){
    	if(categoryDelete['affectedRows']==1){
            return res.send(errorMsgs.customMsg(1,"Success!!! Category Deleted Successfully.",{}));
        }
        else{
            return res.send(errorMsgs.customMsg(-1,"Sorry!!! Category can't be Deleted. Try Again.",{}));
        }
    }
}


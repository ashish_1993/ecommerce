var db = require('../database/Db');
var errorMsgs=require('../config/ErrorMsg');

exports.addBrand = async function addBrand(req,res){
    let image = '';
    if(req.files != undefined){
        image = req.files[0].path;  
    }
    let brand= await db.query("insert into tbl_brand(name, description, image,status) values (?,?,?,?)",[req.body.name,req.body.description,image,req.body.status]);
    if(brand!==undefined && brand!==null){
    	if(brand['affectedRows']==1){
            return res.send(errorMsgs.customMsg(1,"Success!!! Brand Added Successfully.",{}));
        }
        else{
            return res.send(errorMsgs.customMsg(-1,"Sorry!!! Brand can't be Added. Try Again.",{}));
        }
    }
}



exports.getBrand = async function getBrand(req,res){
	var whereClause = req.query;
	let brand = [];
	
	if(Object.keys(whereClause).length && (whereClause.hasOwnProperty("id") || whereClause.id == null || whereClause.id == "")){
		brand= await db.query("select * from tbl_brand where id=?",[whereClause.id]);
	}else if(Object.keys(whereClause).length && whereClause.hasOwnProperty("status")){
        brand= await db.query("select * from tbl_brand where status=?",[whereClause.status]);    
    }
    else{
		brand= await db.query("select * from tbl_brand");
	}
    
    return res.send(errorMsgs.customMsg1(1,"Data Found","brand",brand));
}


exports.deleteBrand = async function deleteBrand(req,res){
	let brandDelete= await db.query("DELETE FROM `tbl_brand` WHERE id=?",[req.body.id]);

    if(brandDelete!==undefined && brandDelete!==null){
    	if(brandDelete['affectedRows']==1){
            return res.send(errorMsgs.customMsg(1,"Success!!! Brand Deleted Successfully.",{}));
        }
        else{
            return res.send(errorMsgs.customMsg(-1,"Sorry!!! Brand can't be Deleted. Try Again.",{}));
        }
    }
}


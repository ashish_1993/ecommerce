var db = require('../database/Db');
var errorMsgs=require('../config/ErrorMsg');

exports.addWatchType = async function addWatchType(req,res){
 
    let watchType= await db.query("insert into tbl_watch_type(type_name,status) values (?,?)",[req.body.type_name,req.body.status]);
    if(watchType!==undefined && watchType!==null){
    	if(watchType['affectedRows']==1){
            return res.send(errorMsgs.customMsg(1,"Success!!! Watch Type Added Successfully.",{}));
        }
        else{
            return res.send(errorMsgs.customMsg(-1,"Sorry!!! Watch Type can't be Added. Try Again.",{}));
        }
    }
}



exports.getWatchType = async function getWatchType(req,res){
	var whereClause = req.query;
	let watchType = [];
	
	if(Object.keys(whereClause).length && (whereClause.hasOwnProperty("type_id") || whereClause.type_id == null || whereClause.type_id == "")){
		watchType= await db.query("select * from tbl_watch_type where type_id=?",[whereClause.type_id]);
	}else if(Object.keys(whereClause).length && whereClause.hasOwnProperty("status")){
        watchType= await db.query("select * from tbl_watch_type where status=?",[whereClause.status]);    
    }
    else{
		watchType= await db.query("select * from tbl_watch_type");
	}
    
    return res.send(errorMsgs.customMsg1(1,"Data Found","watchType",watchType));
}


exports.deleteWatchType = async function deleteWatchType(req,res){
	let watchTypeDelete= await db.query("DELETE FROM `tbl_watch_type` WHERE type_id=?",[req.body.type_id]);

    if(watchTypeDelete!==undefined && watchTypeDelete!==null){
    	if(watchTypeDelete['affectedRows']==1){
            return res.send(errorMsgs.customMsg(1,"Success!!! Watch Type Deleted Successfully.",{}));
        }
        else{
            return res.send(errorMsgs.customMsg(-1,"Sorry!!! Watch Type can't be Deleted. Try Again.",{}));
        }
    }
}


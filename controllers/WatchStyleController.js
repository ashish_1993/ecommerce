var db = require('../database/Db');
var errorMsgs=require('../config/ErrorMsg');

exports.addWatchStyle = async function addWatchStyle(req,res){
 
    let watchStyle= await db.query("insert into tbl_watch_style(style_name,status) values (?,?)",[req.body.style_name,req.body.status]);
    if(watchStyle!==undefined && watchStyle!==null){
        if(watchStyle['affectedRows']==1){
            return res.send(errorMsgs.customMsg(1,"Success!!! Watch Style Added Successfully.",{}));
        }
        else{
            return res.send(errorMsgs.customMsg(-1,"Sorry!!! Watch Style can't be Added. Try Again.",{}));
        }
    }
}



exports.getWatchStyle = async function getWatchStyle(req,res){
    var whereClause = req.query;
    let watchStyle = [];
    
    if(Object.keys(whereClause).length && (whereClause.hasOwnProperty("type_id") || whereClause.type_id == null || whereClause.type_id == "")){
        watchStyle= await db.query("select * from tbl_watch_style where type_id=?",[whereClause.type_id]);
    }else if(Object.keys(whereClause).length && whereClause.hasOwnProperty("status")){
        watchStyle= await db.query("select * from tbl_watch_style where status=?",[whereClause.status]);    
    }
    else{
        watchStyle= await db.query("select * from tbl_watch_style");
    }
    
    return res.send(errorMsgs.customMsg1(1,"Data Found","watchStyle",watchStyle));
}


exports.deleteWatchStyle = async function deleteWatchStyle(req,res){
    let watchStyleDelete= await db.query("DELETE FROM `tbl_watch_style` WHERE type_id=?",[req.body.type_id]);

    if(watchStyleDelete!==undefined && watchStyleDelete!==null){
        if(watchStyleDelete['affectedRows']==1){
            return res.send(errorMsgs.customMsg(1,"Success!!! Watch Style Deleted Successfully.",{}));
        }
        else{
            return res.send(errorMsgs.customMsg(-1,"Sorry!!! Watch Style can't be Deleted. Try Again.",{}));
        }
    }
}


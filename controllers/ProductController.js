var db = require('../database/Db');
var errorMsgs=require('../config/ErrorMsg');

exports.addProduct = async function addProduct(req,res){

    let product= await db.query("insert into tbl_products(product_name,product_description,brand_id,dial_color,function_category,strap_color,strap_material,watch_style,watch_type,available,price,quantity, status) values (?,?,?,?,?,?,?,?,?,?,?,?,?)",[req.body.product_name,req.body.product_description,req.body.brand_id,req.body.dial_color,req.body.function_category,req.body.strap_color,req.body.strap_material,req.body.watch_style,req.body.watch_type,req.body.available,req.body.price,req.body.quantity,req.body.status]);
    if(product!==undefined && product!==null){
    	if(product['affectedRows']==1){
            return res.send(errorMsgs.customMsg(1,"Success!!! Product Added Successfully.",{}));
        }
        else{
            return res.send(errorMsgs.customMsg(-1,"Sorry!!! Product can't be Added. Try Again.",{}));
        }
    }
}



exports.getProduct = async function getProduct(req,res){
	var whereClause = req.query;
	let product = [];
	
	if(Object.keys(whereClause).length && (whereClause.hasOwnProperty("product_id") || whereClause.product_id == null || whereClause.product_id == "")){
		product= await db.query("select * from tbl_products where product_id=?",[whereClause.product_id]);
	}else if(Object.keys(whereClause).length && whereClause.hasOwnProperty("status")){
        product= await db.query("select * from tbl_products where status=?",[whereClause.status]);    
    }
    else{
		product= await db.query("select * from tbl_products");
	}
    
    return res.send(errorMsgs.customMsg1(1,"Data Found","product",product));
}


exports.deleteProduct = async function deleteProduct(req,res){
	let productDelete= await db.query("DELETE FROM `tbl_products` WHERE product_id=?",[req.body.product_id]);

    if(productDelete!==undefined && productDelete!==null){
    	if(productDelete['affectedRows']==1){
            return res.send(errorMsgs.customMsg(1,"Success!!! Product Deleted Successfully.",{}));
        }
        else{
            return res.send(errorMsgs.customMsg(-1,"Sorry!!! Product can't be Deleted. Try Again.",{}));
        }
    }
}


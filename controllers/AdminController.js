var db = require('../database/Db');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var errorMsgs=require('../config/ErrorMsg');


exports.addAdminUser = (req, res) => {
	 bcrypt.genSalt(10,(err,salt)=>{
        if(err){
            return res.status(403).send(err);
        }
        bcrypt.hash(`${req.body.password}`,salt,(err,hash)=>{
            if(err){
                return res.status(403).send(err);
            }
			db.query("INSERT INTO tbl_admin_user(name, email,mobile, password, status) VALUES (?, ?, ?, ?, ?)", [req.body.name, req.body.email,req.body.mobile,hash, req.body.status])
			.then( (result) => {
				if(result.insertId) {
					res.status(200).send({'id':result.insertId});
				} else {
					res.status(200).send({message: "Admin User Not Added"});
				}
			}).catch( (err) => {
				console.log(err);
				if(err.code == 23502) {
					res.status(400).send({message: "Please Provide All Required Data"});
				} else if(err.code == 23505) {
					res.status(400).send({message: "Admin User Already Exists"});
				} else {
					res.status(400).send({message: "Could Not Add Admin User. Please Try Again."});
				}
			});
		});

	});
}


exports.getAdminUser = async function getAdminUser(req,res){
	var whereClause = req.query;
	let adminUser = [];
	
	if(Object.keys(whereClause).length && (whereClause.hasOwnProperty("id") || whereClause.id == null || whereClause.id == "")){
		adminUser= await db.query("select * from tbl_admin_user where id=?",[whereClause.id]);
	}else if(Object.keys(whereClause).length && whereClause.hasOwnProperty("status")){
        adminUser= await db.query("select * from tbl_admin_user where status=?",[whereClause.status]);    
    }
    else{
		adminUser= await db.query("select * from tbl_admin_user");
	}
    
    return res.send(errorMsgs.customMsg1(1,"Data Found","adminUser",adminUser));
}


exports.deleteAdminUser = async function deleteAdminUser(req,res){
	let userDelete= await db.query("DELETE FROM `tbl_admin_user` WHERE id=?",[req.body.id]);

    if(userDelete!==undefined && userDelete!==null){
    	if(userDelete['affectedRows']==1){
            return res.send(errorMsgs.customMsg(1,"Success!!! Admin User Deleted Successfully.",{}));
        }
        else{
            return res.send(errorMsgs.customMsg(-1,"Sorry!!! Admin User can't be Deleted. Try Again.",{}));
        }
    }
}


exports.login = async function login(req,res){
	try{
        let resu = await db.query("select * from tbl_admin_user where email=?",[req.body.email]);
        //console.log(resu)
        if(resu.length != 0){
        	if(resu[0]['status'] == 'Active'){
	        	if(bcrypt.compare(req.body.password,resu[0]['password'],(err,result)=>{
		            if(err){
		                console.log(err);
		                return res.status(403).send(errorMsgs.customMsg(-1,"Something went wrong",{}));
		            }
		            if(result){
		            	delete resu[0].password;
		                 return res.send(errorMsgs.customMsg1(1, 'Login Successfully',"info",resu[0]));
		            }
		            else{
		                return res.send(errorMsgs.customMsg(0,"Invalid Credentials",{}));
		            }
		        }
		        ));
	        }else{
	        	 return res.send(errorMsgs.customMsg(0,"Please User Your Profile Not Active!!!",{}));
	        }
        }else{
        	return res.send(errorMsgs.customMsg(0,"User Not Exits!!!",{}));
        }
        
        
    }catch(err){
        return res.send(err);
    }		
		
}
var db = require('../database/Db');
var errorMsgs=require('../config/ErrorMsg');

exports.addFunctionCategory = async function addFunctionCategory(req,res){
 
    let functionCategory= await db.query("insert into tbl_watch_functions_category(function_name, function_description,status) values (?,?,?)",[req.body.function_name,req.body.function_description,req.body.status]);
    if(functionCategory!==undefined && functionCategory!==null){
    	if(functionCategory['affectedRows']==1){
            return res.send(errorMsgs.customMsg(1,"Success!!! Function Category Added Successfully.",{}));
        }
        else{
            return res.send(errorMsgs.customMsg(-1,"Sorry!!! Function Category can't be Added. Try Again.",{}));
        }
    }
}



exports.getFunctionCategory = async function getFunctionCategory(req,res){
	var whereClause = req.query;
	let functionCategory = [];
	
	if(Object.keys(whereClause).length && (whereClause.hasOwnProperty("function_id") || whereClause.function_id == null || whereClause.function_id == "")){
		functionCategory= await db.query("select * from tbl_watch_functions_category where function_id=?",[whereClause.function_id]);
	}else if(Object.keys(whereClause).length && whereClause.hasOwnProperty("status")){
        functionCategory= await db.query("select * from tbl_watch_functions_category where status=?",[whereClause.status]);    
    }
    else{
		functionCategory= await db.query("select * from tbl_watch_functions_category");
	}
    
    return res.send(errorMsgs.customMsg1(1,"Data Found","functionCategory",functionCategory));
}


exports.deleteFunctionCategory = async function deleteFunctionCategory(req,res){
	let functionCategoryDelete= await db.query("DELETE FROM `tbl_watch_functions_category` WHERE function_id=?",[req.body.function_id]);

    if(functionCategoryDelete!==undefined && functionCategoryDelete!==null){
    	if(functionCategoryDelete['affectedRows']==1){
            return res.send(errorMsgs.customMsg(1,"Success!!! Function Category Deleted Successfully.",{}));
        }
        else{
            return res.send(errorMsgs.customMsg(-1,"Sorry!!! Function Category can't be Deleted. Try Again.",{}));
        }
    }
}


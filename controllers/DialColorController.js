var db = require('../database/Db');
var errorMsgs=require('../config/ErrorMsg');

exports.addDialColor = async function addDialColor(req,res){
 
    let dialColor= await db.query("insert into tbl_watch_dial_color(dial_color_name, dial_color_code,status) values (?,?,?)",[req.body.dial_color_name,req.body.dial_color_code,req.body.status]);
    if(dialColor!==undefined && dialColor!==null){
    	if(dialColor['affectedRows']==1){
            return res.send(errorMsgs.customMsg(1,"Success!!! Dial Color Added Successfully.",{}));
        }
        else{
            return res.send(errorMsgs.customMsg(-1,"Sorry!!! Dial Color can't be Added. Try Again.",{}));
        }
    }
}



exports.getDialColor = async function getDialColor(req,res){
	var whereClause = req.query;
	let dialColor = [];
	
	if(Object.keys(whereClause).length && (whereClause.hasOwnProperty("dial_color_id") || whereClause.dial_color_id == null || whereClause.dial_color_id == "")){
		dialColor= await db.query("select * from tbl_watch_dial_color where dial_color_id=?",[whereClause.dial_color_id]);
	}else if(Object.keys(whereClause).length && whereClause.hasOwnProperty("status")){
        dialColor= await db.query("select * from tbl_watch_dial_color where status=?",[whereClause.status]);    
    }
    else{
		dialColor= await db.query("select * from tbl_watch_dial_color");
	}
    
    return res.send(errorMsgs.customMsg1(1,"Data Found","dialColor",dialColor));
}


exports.deleteDialColor = async function deleteDialColor(req,res){
	let dialColorDelete= await db.query("DELETE FROM `tbl_watch_dial_color` WHERE dial_color_id=?",[req.body.dial_color_id]);

    if(dialColorDelete!==undefined && dialColorDelete!==null){
    	if(dialColorDelete['affectedRows']==1){
            return res.send(errorMsgs.customMsg(1,"Success!!! Dial Color Deleted Successfully.",{}));
        }
        else{
            return res.send(errorMsgs.customMsg(-1,"Sorry!!! Dial Color can't be Deleted. Try Again.",{}));
        }
    }
}


const multer = require("multer");
const imagemin = require('imagemin');
var path = require('path');
const imageminMozjpeg = require('imagemin-mozjpeg');
const imageminPngquant = require('imagemin-pngquant');

var multerStorage = multer.diskStorage({
	destination: function(req, file, callback) {
        let imagePath = './uploads/'
        if(req.url === "/addBrand"){
         imagePath = "./uploads/brands/";
        }
		callback(null, imagePath);
	},
	filename: function(req, file, callback) {
		callback(null, file.originalname);
	},
	
});

const upload = multer({
  storage: multerStorage,
  fileFilter: function (req, file, callback) {
    var ext = path.extname(file.originalname);
    if(ext !== '.png' && ext !== '.jpg' && ext !== '.jpeg'&& ext !== '.HEIC' && ext !== '.JPG' && ext !== '.PNG' && ext !== '.JPEG') {
        return callback(new Error('Only images are allowed'))
    }
    callback(null, true)
}
});

const uploadFiles = upload.array("image", 5);

exports.uploadImages = (req, res, next) => {
//console.log(req.body);
  uploadFiles(req, res, err => {
    if (err instanceof multer.MulterError) { // A Multer error occurred when uploading.
      if (err.code === "LIMIT_UNEXPECTED_FILE") { // Too many images exceeding the allowed limit
        
      }
    } else if (err) {
      
    }

    next();
  });
};